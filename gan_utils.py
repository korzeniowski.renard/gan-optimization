import os
from functools import partial
from pathlib import Path

import PIL
import numpy as np
import matplotlib.pyplot as plt
import torch
import torchvision.utils as vutils
from torchvision.datasets import FashionMNIST, LSUN, VisionDataset
from torchvision import transforms
from torch.utils.data import DataLoader
from shutil import copyfile

from torchvision.datasets.utils import verify_str_arg

import hparams


class CelebADataset(VisionDataset):
    def __init__(self, root, split="train", target_type="attr", transform=None,
                 target_transform=None):
        import pandas
        super(CelebADataset, self).__init__(root, transform=transform,
                                            target_transform=target_transform)
        self.split = split
        self.base_folder = 'celeba-dataset'
        if isinstance(target_type, list):
            self.target_type = target_type
        else:
            self.target_type = [target_type]

        if not self.target_type and self.target_transform is not None:
            raise RuntimeError('target_transform is specified but target_type is empty')

        split_map = {
            "train": 0,
            "valid": 1,
            "test": 2,
            "all": None,
        }
        split = split_map[verify_str_arg(split.lower(), "split",
                                         ("train", "valid", "test", "all"))]

        fn = partial(os.path.join, self.root, self.base_folder)
        splits = pandas.read_csv(fn("list_eval_partition.csv"), index_col=0)
        # identity = pandas.read_csv(fn("identity_CelebA.csv"), delim_whitespace=True, header=None, index_col=0)
        bbox = pandas.read_csv(fn("list_bbox_celeba.csv"), index_col=0)
        landmarks_align = pandas.read_csv(fn("list_landmarks_align_celeba.csv"), index_col=0)
        attr = pandas.read_csv(fn("list_attr_celeba.csv"), index_col=0)

        mask = slice(None) if split is None else (splits['partition'] == split)

        self.filename = splits[mask].index.values
        # self.identity = torch.as_tensor(identity[mask].values)
        self.bbox = torch.as_tensor(bbox[mask].values)
        self.landmarks_align = torch.as_tensor(landmarks_align[mask].values)
        self.attr = torch.as_tensor(attr[mask].values)
        self.attr = (self.attr + 1) // 2  # map from {-1, 1} to {0, 1}
        self.attr_names = list(attr.columns)

    def __getitem__(self, index):
        x = PIL.Image.open(os.path.join(self.root, self.base_folder,
                                        "img_align_celeba/img_align_celeba",
                                        self.filename[index]))

        # target = []
        # for t in self.target_type:
        #     if t == "attr":
        #         target.append(self.attr[index, :])
        #     elif t == "identity":
        #         target.append(self.identity[index, 0])
        #     elif t == "bbox":
        #         target.append(self.bbox[index, :])
        #     elif t == "landmarks":
        #         target.append(self.landmarks_align[index, :])
        #     else:
        #         # TODO: refactor with utils.verify_str_arg
        #         raise ValueError("Target type \"{}\" is not recognized.".format(t))
        #
        if self.transform is not None:
            x = self.transform(x)
        #
        # if target:
        #     target = tuple(target) if len(target) > 1 else target[0]
        #
        #     if self.target_transform is not None:
        #         target = self.target_transform(target)
        # else:
        #     target = None

        return x, torch.zeros(1)

    def __len__(self):
        return len(self.attr)


class data_loading:

    @staticmethod
    def get_data_loader(loader_type, data_path, bs, image_size):
        if loader_type == 'FMNIST':
            dataset = FashionMNIST(data_path,
                                   download=True,
                                   transform=transforms.Compose([transforms.Resize(image_size),
                                                                 transforms.ToTensor()]))

        elif loader_type == 'CELEBA':
            if not os.path.exists(Path(data_path) / 'celeba-dataset'):
                print('CELEBA not found. Downloading...')
                if hparams.kaggle_username == '' or hparams.kaggle_key == '':
                    raise RuntimeError('kaggle_username and kaggle_key must be set to download CELEBA.'
                                       'Visit https://www.kaggle.com/yourusername to get your key.')
                os.environ['KAGGLE_USERNAME'] = hparams.kaggle_username
                os.environ['KAGGLE_KEY'] = hparams.kaggle_key

                import kaggle

                kaggle.api.authenticate()
                kaggle.api.dataset_download_files('jessicali9530/celeba-dataset',
                                                  path=f'{data_path}/celeba-dataset',
                                                  unzip=True)

                copyfile('./identity_CelebA.txt', f'{data_path}/celeba-dataset/identity_CelebA.csv')

            dataset = CelebADataset(
                Path(data_path),
                transform=transforms.Compose([
                    transforms.Resize(image_size),
                    transforms.CenterCrop(image_size),
                    transforms.ToTensor(),
                    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
                ])
                )
        elif loader_type == 'LSUN':
            if not os.path.exists(Path(data_path) / f'{hparams.lsun_selected_category}_train_lmdb'):
                print('LSUN not found. Downloading...')
                import lsun_download
                lsun_download.main()
            dataset = LSUN(data_path,
                           classes=[hparams.lsun_selected_category + '_train'],
                           transform=transforms.Compose([transforms.Resize((image_size, image_size)),
                                                         transforms.ToTensor()]))
        else:
            raise RuntimeError(f'Unknown loader_type {loader_type}!')

        data_loader = DataLoader(dataset, batch_size=bs, shuffle=True)
        return data_loader


def plot_gan_training_results(real_batch, fake_imgs, gen_losses, disc_losses, device):

    plt.figure(figsize=(10, 5))
    plt.title("Generator and Discriminator Loss During Training")
    plt.plot(gen_losses, label="G")
    plt.plot(disc_losses, label="D")
    plt.xlabel("iterations")
    plt.ylabel("Loss")
    plt.legend()
    plt.show()

    plt.figure(figsize=(15, 15))
    plt.subplot(1, 2, 1)
    plt.axis("off")
    plt.title("Real Images")
    plt.imshow(
        np.transpose(vutils.make_grid(
            real_batch[0].to(device)[:128],
            padding=5,
            normalize=True
        ).cpu(), (1, 2, 0)))

    plt.subplot(1, 2, 2)
    plt.axis("off")
    plt.title("Fake Images")
    plt.imshow(np.transpose(fake_imgs, (1, 2, 0)))
    plt.show()

from PIL import Image
import torch
import torchvision.utils as vutils
import matplotlib.pyplot as plt
import numpy as np
import hparams


class DCGAN:
    """
            /---------------------<<<<<----------------------\
    1. generator ---> fake_img                            gen_loss
                          +    --->   discriminator  --->    +
                         img                |             disc_loss
                                            ------<<<<<------/
    """
    def __init__(
            self,
            model_name,
            disc_steps,
            generator,
            discriminator,
            gen_loss_fn,
            disc_loss_fn,
            gen_optimizer,
            disc_optimizer,
            data_loader,
            device,
            seed,
            params,
            disc_lr_scheduler=None,
            gen_lr_scheduler=None,
    ):
        self.model_name = model_name
        self.generator = generator
        self.discriminator = discriminator
        self.gen_loss_fn = gen_loss_fn
        self.gen_optimizer = gen_optimizer
        self.disc_loss_fn = disc_loss_fn
        self.disc_optimizer = disc_optimizer
        self.latent_shape = hparams.latent_shape
        self.gen_lr_scheduler = gen_lr_scheduler
        self.disc_lr_scheduler = disc_lr_scheduler

        self.data_loader = data_loader
        self.epochs = hparams.epochs
        self.log_interval = hparams.log_interval
        self.ds_name = hparams.loader_type
        self.seed = seed
        self.suffix = params.name_suffix
        self.counter = 0

        self.gen_losses = []
        self.disc_losses = []
        self.disc_target = hparams.disc_target
        self.gen_target = hparams.gen_target
        self.gen_steps = hparams.gen_steps
        self.disc_steps = disc_steps
        self.device = device

        self.fixed_noise = None
        self.img_white_noise = None

        self.experience_buffer = []

    def train_model(self):
        """latent_features should come from 100 dim uniform distribution [0, 1)"""
        self.generator.train()
        self.discriminator.train()
        for epoch in range(self.epochs):
            self.run_epoch(epoch)
            self.save_img(epoch)
            if hparams.save_model: self.save_model(epoch)

    def run_epoch(self, epoch):
        self.counter = 0
        for sample in self.data_loader:
            img, label = sample

            img = img.to(self.device)
            label = label.to(self.device)

            logging_data = self._run_epoch(img, label)

            self.log_loss(epoch, logging_data)

            if self.counter % self.disc_steps == 0:
                if self.gen_lr_scheduler: self.gen_lr_scheduler.step()
                if self.disc_lr_scheduler: self.disc_lr_scheduler.step()
            self.counter += 1

    def _run_epoch(self, img, label):
        latent_features = self.generate_latent_features(img.shape[0], label=label)
        img = self.process_real_img(img, label)
        gen_img = self.generate_img(latent_features, label=label, extend_history=True)

        log_disc_data = self.forward_disc(
            img=img, gen_img=gen_img)
        log_gen_data = self.forward_gen(
            gen_img=gen_img, latent_features=latent_features)
        return log_disc_data, log_gen_data

    def forward_disc(self, img, gen_img, **kwargs):
        self.discriminator.zero_grad()
        pred_real_img = self.discriminator(img).reshape(-1)
        label_real_img = torch.ones(pred_real_img.shape[0], device=self.device) * self.disc_target
        real_img_loss = self.disc_loss_fn(pred_real_img, label_real_img)
        real_img_loss.backward()

        pred_gen_img = self.discriminator(gen_img.detach()).reshape(-1)
        label_gen_img = torch.zeros(pred_gen_img.shape[0], device=self.device)
        gen_img_loss = self.disc_loss_fn(pred_gen_img, label_gen_img)
        gen_img_loss.backward()

        D_x = pred_real_img.mean().item()
        D_G_z1 = pred_gen_img.mean().item()

        disc_loss = gen_img_loss + real_img_loss

        if self.counter % self.disc_steps == 0:
            self.disc_optimizer.step()
            self.disc_losses.append(disc_loss.item())

        return D_x, D_G_z1

    def forward_gen(self, gen_img, latent_features, **kwargs):
        D_G_z2 = np.nan
        if self.counter % self.disc_steps == 0:
            for _ in range(self.gen_steps):
                self.generator.zero_grad()
                pred_gen_img = self.discriminator(gen_img).reshape(-1)
                label_gen_img = torch.ones(pred_gen_img.shape[0], device=self.device) * self.gen_target
                gen_loss = self.gen_loss_fn(pred_gen_img, label_gen_img)
                gen_loss.backward()

                D_G_z2 = pred_gen_img.mean().item()
                self.gen_optimizer.step()
                if self.gen_steps > 1:
                    gen_img = self.generate_img(latent_features)

                self.gen_losses.append(gen_loss.item())
        return D_G_z2

    def generate_img(self, latent_features, label=None, extend_history=False):
        try:
            img = self.generator(latent_features)
        except:
            import pdb;
            pdb.set_trace()
        if hparams.add_noise:
            img = img + hparams.noise_intensity * self.img_white_noise
        if hparams.experience_replay:
            img = self.add_old_generator_iterations(img, extend_history)
        return img

    def add_old_generator_iterations(self, img, extend_history):
        if self.is_extend_experience_buffer(extend_history):
            self.experience_buffer.insert(0, img.detach())
            if self.is_experience_buffer_overflowed():
                self.experience_buffer.pop()
        extended_img = torch.cat([img] + self.experience_buffer)
        return extended_img

    def process_real_img(self, img, label, **kwargs):
        img = self.add_white_noise(img)
        return img

    def add_white_noise(self, img):
        if hparams.add_noise:
            self.img_white_noise = torch.randn(
                img.shape,
                device=self.device,
                requires_grad=False
            ).clamp_(-1, 1)
            img = img + hparams.noise_intensity * self.img_white_noise
        return img

    def is_experience_buffer_overflowed(self):
        return len(self.experience_buffer) > hparams.generator_history_len

    def is_extend_experience_buffer(self, extend_history):
        return self.counter % hparams.experience_replay_iter_spacing == 0 and extend_history

    def log_loss(self, epoch, logging_data):
        log_disc_data, log_gen_data = logging_data
        D_x, D_G_z1 = log_disc_data
        D_G_z2 = log_gen_data

        if self.counter % hparams.log_interval == 0:
            print('[%d/%d][%d/%d]\tLoss_D: %.4f\tLoss_G: %.4f\tD(x): %.4f\tD(G(z)): %.4f / %.4f'
                  % (epoch, self.epochs, self.counter, len(self.data_loader),
                     self.disc_losses[-1], self.gen_losses[-1], D_x, D_G_z1, D_G_z2))

    def generate_latent_features(self, batch_size, **kwargs):
        return torch.randn(batch_size, self.latent_shape, 1, 1, device=self.device)

    def save_img(self, epoch):
        if self.fixed_noise is None:
            self.fixed_noise = self.generate_latent_features(hparams.n_saved_images)

        model_name, optimizer = self.model_name.split("_")
        with torch.no_grad():
            fake = self.generator(self.fixed_noise).detach().cpu()
        path = hparams.imgs_save_path.format(
            ds_name=self.ds_name,
            epoch=epoch,
            optimizer=optimizer,
            loss_fn=model_name,
            seed=self.seed,
            suffix=self.suffix,
        )
        vutils.save_image(fake, fp=path, padding=2, nrow=hparams.img_rows, normalize=True)

    def save_model(self, epoch):
        model_name, optimizer = self.model_name.split("_")
        torch.save(
            {
                "generator_state_dict": self.generator.state_dict(),
                "discriminator_state_dict": self.discriminator.state_dict(),
            },
            hparams.full_model_save_path.format(
                ds_name=self.ds_name,
                epoch=epoch,
                optimizer=optimizer,
                loss_fn=model_name,
                seed=self.seed,
                suffix=self.suffix,
            )
        )
        torch.save(self.fixed_noise, hparams.noise_save_path)

    def save_full_model(self, ds_name, optimizer, loss_fn, seed):
        torch.save(
            {
                "generator_state_dict": self.generator.state_dict(),
                "discriminator_state_dict": self.discriminator.state_dict(),
            },
            hparams.full_model_save_path.format(
                ds_name=ds_name,
                epoch=hparams.epochs,
                optimizer=optimizer,
                loss_fn=loss_fn,
                seed=seed,
            )
        )


class WGAN(DCGAN):
    def __init__(self, params, **kwargs):
        super().__init__(params=params, **kwargs)
        self.weight_clipping = params.weight_clipping

    def forward_gen(self, gen_img, **kwargs):
        gen_loss = np.nan
        if self.counter % self.disc_steps == 0:
            self.generator.zero_grad()
            gen_loss = -torch.mean(self.discriminator(gen_img))

            gen_loss.backward()
            self.gen_optimizer.step()

            self.gen_losses.append(gen_loss)#.item())
        return gen_loss #.item()

    def forward_disc(self, img, gen_img, **kwargs):
        self.discriminator.zero_grad()
        gen_img = gen_img.detach()

        adversarial_loss = -torch.mean(self.discriminator(img)) + torch.mean(self.discriminator(gen_img))
        disc_loss = adversarial_loss
        disc_loss.backward()
        self.disc_optimizer.step()
        for param in self.discriminator.parameters():
            param.data.clamp_(-self.weight_clipping, self.weight_clipping)

        if self.counter % self.disc_steps == 0:
            self.disc_losses.append(disc_loss.item())

        return disc_loss.item(), gen_img.mean().item()


class WGANGP(DCGAN):
    def __init__(self, params, **kwargs):
        super().__init__(params=params, **kwargs)
        self.grad_penalty_coefficient = params.grad_penalty

    def get_gradient_penalty(self, gen_img, real_img):
        alpha = torch.randn(real_img.shape[0], 1, 1, 1, requires_grad=True, device=self.device)

        img_mix = alpha * real_img + (1 - alpha) * gen_img

        pred = self.discriminator(img_mix)

        labels = torch.ones(real_img.shape[0], 1, 1, 1, requires_grad=False, device=self.device)

        gradient = torch.autograd.grad(
            outputs=pred,
            inputs=img_mix,
            grad_outputs=labels,
            retain_graph=True,
            create_graph=True,
            only_inputs=True,
        )[0]

        gradient = gradient.view(gradient.size(0), -1)
        grad_penalty = ((gradient.norm(2, dim=1) - 1)**2).mean()
        return grad_penalty

    def forward_disc(self, img, gen_img, **kwargs):
        for p in self.discriminator.parameters():
            p.requires_grad = True

        for _ in range(self.disc_steps):
            self.discriminator.zero_grad()
            gen_img = gen_img.detach()

            d_real = torch.mean(self.discriminator(img))
            d_real.backward(-torch.ones(1, device=self.device).squeeze())

            d_fake = torch.mean(self.discriminator(gen_img))
            d_fake.backward(torch.ones(1, device=self.device).squeeze())

            grad_penalty = self.get_gradient_penalty(gen_img=gen_img, real_img=img)
            grad_penalty.backward()

            d_cost = d_fake + d_real + grad_penalty
            self.disc_optimizer.step()

        self.disc_losses.append(d_cost)
        return d_cost.item(), gen_img

    def get_current_iteration_samples(self, gen_img, batch_size):
        return gen_img[:batch_size]

    def forward_gen(self, gen_img, **kwargs):
        for p in self.discriminator.parameters():
            p.requires_grad = False

        self.generator.zero_grad()
        gen_loss = torch.mean(self.discriminator(gen_img))

        gen_loss.backward(-torch.ones(1, device=self.device).squeeze())
        self.gen_optimizer.step()

        self.gen_losses.append(gen_loss)

        return gen_loss

    def log_loss(self, epoch, logging_data):
        disc_loss_data, gen_loss = logging_data
        disc_loss, gen_img = disc_loss_data
        if self.counter % hparams.log_interval == 0:
            print(
                "[%d/%d] [%d/%d] [D loss: %f] [G loss: %f]"
                % (epoch, self.epochs, self.counter % len(self.data_loader),
                   len(self.data_loader), disc_loss, gen_loss)
            )

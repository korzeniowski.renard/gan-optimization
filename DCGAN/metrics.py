import torch
from torch import nn
from torch.nn import functional as F
import torch.utils.data
import cv2
import numpy as np
from scipy.linalg import sqrtm
from tqdm import tqdm

import hparams
from DCGAN.gs import rlt, geom_score


def _to_3_channels(imgs):
    _, channels, x, y = imgs.shape
    if channels == 1:
        imgs2 = []
        for img in imgs:
            img = img.reshape(x, y, channels)
            img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
            img = img.reshape(3, x, y)
            imgs2.append(img)
        imgs = np.stack(imgs2, axis=0)
    return imgs


def get_metrics(generator, inception_model, data_loader, device, batch_size, resize=True, return_baseline=False):
    n = hparams.evaluation_bs*hparams.evaluation_no_iterations

    assert batch_size > 0
    assert n > batch_size

    inception_model.eval()
    up = nn.Upsample(size=(299, 299), mode='bilinear', align_corners=True).to(device)

    def get_pred(x):
        if resize:
            x = up(x)
        x = inception_model(x)
        return F.softmax(x, dim=1).data.cpu().numpy()

    # Get predictions
    p_pred = np.zeros((n, 1000))
    p_actual = np.zeros((n, 1000))
    gs_pred = np.zeros((hparams.evaluation_no_iterations, 100))
    gs_actual = np.zeros((hparams.evaluation_no_iterations, 100))

    for i, (imgs_actual, _) in zip(tqdm(range(hparams.evaluation_no_iterations)), data_loader):
        latent_features = torch.randn(hparams.evaluation_bs, hparams.latent_shape, 1, 1, device=device)
        imgs = generator(latent_features)
        imgs = torch.tensor(_to_3_channels(imgs.detach().cpu().numpy()), device=device)
        imgs_actual = torch.tensor(_to_3_channels(imgs_actual.detach().cpu().numpy()), device=device)
        p_pred[(i * hparams.evaluation_bs): (i+1) * hparams.evaluation_bs] = get_pred(imgs)
        p_actual[(i * hparams.evaluation_bs): (i + 1) * hparams.evaluation_bs] = get_pred(imgs_actual)

        bs, channels, x, y = imgs.shape
        gs_pred[i] = rlt(imgs.reshape(bs, channels * x * y).T.detach().cpu().numpy())
        gs_actual[i] = rlt(imgs_actual.reshape(bs, channels * x * y).T.detach().cpu().numpy())

    def calculate_inception_score(p_yx, eps=1E-16):
        # calculate p(y)
        p_y = np.expand_dims(p_yx.mean(axis=0), 0)
        # kl divergence for each image
        kl_d = p_yx * (np.log(p_yx + eps) - np.log(p_y + eps))
        # sum over classes
        sum_kl_d = kl_d.sum(axis=1)
        # average over images
        avg_kl_d = np.mean(sum_kl_d)
        # undo the logs
        is_score = np.exp(avg_kl_d)
        return is_score

    def calculate_fid(p_generated, p_true):
        # calculate activations
        # calculate mean and covariance statistics
        mu1, sigma1 = p_generated.mean(axis=0), np.cov(p_generated, rowvar=False)
        mu2, sigma2 = p_true.mean(axis=0), np.cov(p_true, rowvar=False)
        # calculate sum squared difference between means
        ssdiff = np.sum((mu1 - mu2) ** 2.0)
        # calculate sqrt of product between cov
        covmean = sqrtm(sigma1.dot(sigma2))
        # check and correct imaginary numbers from sqrt
        if np.iscomplexobj(covmean):
            covmean = covmean.real
        # calculate score
        fid = ssdiff + np.trace(sigma1 + sigma2 - 2.0 * covmean)
        return fid

    if return_baseline:
        return calculate_inception_score(p_pred), calculate_fid(p_pred, p_actual),\
               geom_score(gs_pred, gs_actual), calculate_inception_score(p_actual[:n//2]),\
               calculate_fid(p_actual[:n//2], p_actual[n//2:]),\
               geom_score(gs_actual[:hparams.evaluation_no_iterations//2], gs_actual[hparams.evaluation_no_iterations//2:])
    else:
        return calculate_inception_score(p_pred), calculate_fid(p_pred, p_actual), geom_score(gs_pred, gs_actual)

from torch import nn

import hparams


def weights_init(model):
    classname = model.__class__.__name__
    if classname.find('Conv') != -1:
        nn.init.normal_(model.weight.data, hparams.weight_init_mean, hparams.weight_init_var)
    elif classname.find('BatchNorm') != -1:
        nn.init.normal_(model.weight.data, hparams.weight_init_mean_bn, hparams.weight_init_var)
        nn.init.normal_(model.bias.data, 0)


class BatchFixedNorm(nn.Module):
    def __init__(self):
        super().__init__()
        self.mean = hparams.dataset_stats['mean']
        self.var = hparams.dataset_stats['var']

    def forward(self, input_):
        return (input_ - self.mean) / (self.var**(1/2))


class BasicBlock(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, stride, padding, act, frac_conv=False, bn=True, spectral_norm=False):
        super().__init__()
        conv_cls = nn.ConvTranspose2d if frac_conv else nn.Conv2d
        self.conv = conv_cls(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=kernel_size,
            stride=stride,
            padding=padding,
            bias=False,
        )
        if spectral_norm:
            self.conv = nn.utils.spectral_norm(self.conv)

        self.bn = self.get_batch_norm(bn, out_channels)
        self.act = act

    def get_batch_norm(self, batch_norm, out_channels):
        if batch_norm:
            if hparams.trainable_bn:
                bn = nn.BatchNorm2d(num_features=out_channels)
            else:
                bn = BatchFixedNorm()
        else:
            bn = None
        return bn

    def forward(self, X):
        out = self.conv(X)
        if self.bn: out = self.bn(out)
        out = self.act(out)
        return out


class Discriminator(nn.Module):
    def __init__(self, channels_shapes, spectral_norm, disc_bn, kernel_size=hparams.kernel_size,
                 strides=hparams.disc_strides, paddings=hparams.disc_paddings):
        super().__init__()
        ins, outs = channels_shapes[:-1], channels_shapes[1:]
        acts = (len(ins)-1) * [nn.LeakyReLU(negative_slope=0.2)] + [nn.Sigmoid()]

        if disc_bn:
            batch_norms = [False] + [True] * (len(strides) - 2) + [False]
        else:
            batch_norms = [False] * len(strides)

        self.layers = nn.ModuleList([
            BasicBlock(input_shape, output_shape,
                kernel_size=kernel_size, stride=stride, padding=padding, act=act, bn=bn, spectral_norm=spectral_norm)
            for input_shape, output_shape, stride, padding, act, bn
            in zip(ins, outs, strides, paddings, acts, batch_norms)
        ])

    def forward(self, X):
        for layer in self.layers:
            X = layer.forward(X)
        return X


class Generator(nn.Module):
    def __init__(self, channels_shapes, kernel_size=hparams.kernel_size,
                 strides=hparams.gen_strides, paddings=hparams.gen_paddings,
                 batch_norms=hparams.gen_bn):
        super().__init__()
        ins, outs = channels_shapes[:-1], channels_shapes[1:]
        acts = (len(ins)-1) * [nn.ReLU()] + [nn.Tanh()]

        self.layers = nn.ModuleList([
            BasicBlock(input_shape, output_shape, frac_conv=True,
                       kernel_size=kernel_size, stride=stride, padding=padding, act=act, bn=bn)
            for input_shape, output_shape, stride, padding, act, bn in
            zip(ins, outs, strides, paddings, acts, batch_norms)
        ])

    def forward(self, X):
        for layer in self.layers:
            X = layer.forward(X)
        return X

SEED = 420

### General
# possible data sources: FMNIST, CELEBA, LSUN
loader_type = 'CELEBA'
lsun_selected_category = 'classroom'
data_path = '/kaggle/input'
full_model_save_path = '/kaggle/input/gan-opt-models-celeba/{ds_name}_ep{epoch}_{optimizer}_{loss_fn}_seed{seed}{suffix}.pkl'
disc_save_path = './discriminator_ep{epoch}.pytorch'
gen_save_path = './generator_ep{epoch}.pytorch'
noise_save_path = './noise.pytorch'
imgs_save_path = './{ds_name}_ep{epoch}_{optimizer}_{loss_fn}_seed{seed}{suffix}.jpg'
save_model = True

### Logging
log_interval = 100
save_metric_interval = 200
plot_training_progress = True
img_rows = 4
n_saved_images = int(img_rows**2)

### Loading Normalization
loading_normalization_mean = 0.5
loading_normalization_var = 0.5
in_channels = 1 if loader_type == 'FMNIST' else 3

### Common Parameters
epochs = 10
betas = (0.5, 0.999)
bs = 128
dropout_p = 0
weight_init_var = 0.02
weight_init_mean = 0.
weight_init_mean_bn = 1.
image_size = 64
kernel_size = 4
add_noise = False
noise_intensity = 1.0

# metric score
evaluation_bs = 32
evaluation_no_iterations = 40
evaluation_results_path = './results.csv'

### Fixed batchnorm
trainable_bn = True
dataset_stats = {
    'FMNIST': {'mean': -0.7386, 'var': 5.5634e-05},
    'CELEBA': {'mean': -0.1289, 'var': 0.0006},
    'LSUN': {'mean': -0.0051, 'var': 0.0003},
}[loader_type]

### exp replay
experience_replay = False
experience_replay_iter_spacing = 500
generator_history_len = 5

### WGANGP
weight_clipping = 0.1
grad_penalty = 5.0

### Gen Parameters
latent_shape = 100
gen_lr = 0.0002
gen_channels_shapes = [latent_shape, 512, 256, 128, 64, in_channels]
gen_strides = [1, 2, 2, 2, 2]
gen_paddings = [0, 1, 1, 1, 1]
gen_bn = [True] * (len(gen_strides) - 1) + [False]
gen_steps = 1
gen_target = 1.0

### Disc Parameters
disc_lr = gen_lr
disc_channels_shapes = [in_channels, 64, 128, 256, 512, 1]
disc_strides = [2, 2, 2, 2, 1]
disc_paddings = [1, 1, 1, 1, 0]
# SN/WGAN/GP change all to False else [F, T, T,..., F]
# disc_bn = [False] + [True] * (len(strides) - 2) + [False]
disc_target = 1.0
# disc_steps = 5
# disc_spectral_norm = False

# validate correct input shapes
for params in [disc_strides, disc_paddings]:
    assert len(disc_channels_shapes) - 1 == len(params)

for params in [gen_strides, gen_paddings, gen_bn]:
    assert len(gen_channels_shapes) - 1 == len(params)

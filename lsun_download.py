# -*- coding: utf-8 -*-

from __future__ import print_function, division

import os
from os.path import join
import zipfile

import subprocess
from urllib.request import Request, urlopen

import hparams

__author__ = 'Fisher Yu'
__email__ = 'fy@cs.princeton.edu'
__license__ = 'MIT'


def list_categories():
    url = 'http://dl.yf.io/lsun/categories.txt'
    with urlopen(Request(url)) as response:
        return response.read().decode().strip().split('\n')


def download(out_dir, category, set_name):
    url = 'http://dl.yf.io/lsun/scenes/{category}_' \
          '{set_name}_lmdb.zip'.format(**locals())
    if set_name == 'test':
        out_name = 'test_lmdb.zip'
        url = 'http://dl.yf.io/lsun/scenes/{set_name}_lmdb.zip'
    else:
        out_name = '{category}_{set_name}_lmdb.zip'.format(**locals())
    out_path = join(out_dir, out_name)
    cmd = ['curl', url, '-o', out_path]
    print('Downloading', category, set_name, 'set')
    subprocess.call(cmd)


def main():

    filename = hparams.data_path
    if not os.path.exists(filename):
        os.mkdir(filename)
    categories = list_categories()
    if hparams.lsun_selected_category is None:
        print('Downloading', len(categories), 'categories')
        for category in categories:
            download(filename, category, 'train')
            download(filename, category, 'val')
        download(filename, '', 'test')
    else:
        if hparams.lsun_selected_category == 'test':
            download(filename, '', 'test')
        elif hparams.lsun_selected_category not in categories:
            print('Error:', hparams.lsun_selected_category, "doesn't exist in", 'LSUN release')
        else:
            download(filename, hparams.lsun_selected_category, 'train')
            download(filename, hparams.lsun_selected_category, 'val')
    train_zip_path = f'{hparams.data_path}/{hparams.lsun_selected_category}_train_lmdb.zip'
    val_zip_path = f'{hparams.data_path}/{hparams.lsun_selected_category}_val_lmdb.zip'
    with zipfile.ZipFile(train_zip_path, 'r') as zip_ref:
        zip_ref.extractall(hparams.data_path)
    with zipfile.ZipFile(val_zip_path, 'r') as zip_ref:
        zip_ref.extractall(hparams.data_path)
    os.remove(train_zip_path)
    os.remove(val_zip_path)


if __name__ == '__main__':
    main()

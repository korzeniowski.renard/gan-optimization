import torch
import os
from pathlib import Path
import pandas as pd
import re
from torchvision.models.inception import inception_v3

from DCGAN import models
import hparams
from DCGAN.metrics import get_metrics
from gan_utils import data_loading


def calculate_scores():
    if torch.cuda.is_available():
        device = torch.device("cuda:0")
    else:
        print("warning: using cpu")
        device = torch.device("cpu")
    path = Path(hparams.full_model_save_path)
    results = []
    first_of_dataset = set()
    for filename in os.listdir(path.parent):
        m = re.match(r'(?P<ds_name>[a-z]+)_ep(?P<epoch>[0-9]+)_(?P<optimizer>[a-z]+)'
                     r'_(?P<loss_fn>[a-z]+)_seed(?P<seed>[0-9]+).*\.pkl', filename, re.IGNORECASE)

        if m is not None:
            elems = m.groupdict()
            ds_name = elems['ds_name']
            epoch = int(elems['epoch'])
            optimizer = elems['optimizer']
            loss_fn = elems['loss_fn']
            seed = int(elems['seed'])
            file = torch.load(str(path.parent / filename))
            generator = models.Generator(channels_shapes=hparams.gen_channels_shapes).to(device)
            generator.load_state_dict(file['generator_state_dict'])
            generator.eval()

            data_loader = data_loading.get_data_loader(
                loader_type=ds_name,
                data_path=hparams.data_path,
                bs=hparams.evaluation_bs,
                image_size=hparams.image_size
            )

            inception_model = inception_v3(pretrained=True, transform_input=False).to(device)

            print(filename)
            if ds_name in first_of_dataset:
                return_baseline = False
            else:
                return_baseline = True
                first_of_dataset.add(ds_name)
            metrics = get_metrics(generator, batch_size=hparams.evaluation_bs,
                                       inception_model=inception_model, data_loader=data_loader,
                                       device=device, return_baseline=return_baseline)
            if return_baseline:
                is_, fid, gs, is_baseline, fid_baseline, gs_baseline = metrics
                row = [ds_name, 'PLACEHOLDER', 'BASELINE', -1, -1, is_baseline, fid_baseline, gs_baseline]
                results.append(row)
            else:
                is_, fid, gs = metrics
            row = [ds_name, optimizer, loss_fn, seed, epoch, is_, fid, gs]
            results.append(row)
    results = pd.DataFrame(results)
    results.columns = ['dataset_name', 'optimizer', 'loss_fn', 'seed', 'epoch',
                       'mean Inception Score', 'mean Fréchet Inception Distance', 'mean Geometric Score']
    results.to_csv(hparams.evaluation_results_path)


if __name__ == '__main__':
    calculate_scores()

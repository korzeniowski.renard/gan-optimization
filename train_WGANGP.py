import random
import numpy as np
import torch
import torchvision.utils as vutils
import matplotlib.pyplot as plt

from gan_utils import data_loading
import hparams
from DCGAN import models
from DCGAN import pipeline
from DCGAN import losses

np.random.seed(hparams.SEED)
random.seed(hparams.SEED)
torch.manual_seed(hparams.SEED)


if __name__ == "__main__":
    device = torch.device("cuda:0" if (torch.cuda.is_available() and 1 > 0) else "cpu")

    data_loader = data_loading.get_data_loader(
        loader_type=hparams.loader_type,
        data_path=hparams.data_path,
        bs=hparams.bs,
        image_size=hparams.image_size
    )

    generator = models.Generator(channels_shapes=hparams.gen_channels_shapes).to(device)
    discriminator = models.Discriminator(
        channels_shapes=hparams.disc_channels_shapes, spectral_norm=hparams.disc_spectral_norm).to(device)

    generator.apply(models.weights_init)
    discriminator.apply(models.weights_init)

    gen_optimizer = torch.optim.RMSprop(generator.parameters(), lr=hparams.gen_lr)
    disc_optimizer = torch.optim.RMSprop(discriminator.parameters(), lr=hparams.disc_lr)

    gen_loss_fn = losses.WassersteinGeneratorLoss()
    disc_loss_fn = losses.WassersteinDiscriminatorLoss()

    wgan = pipeline.WGANGP(
        generator=generator,
        discriminator=discriminator,
        gen_loss_fn=gen_loss_fn,
        disc_loss_fn=disc_loss_fn,
        gen_optimizer=gen_optimizer,
        disc_optimizer=disc_optimizer,
        data_loader=data_loader,
        device=device,
    )

    wgan.train_model()

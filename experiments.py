from copy import deepcopy
from functools import partial
import random

import numpy as np
import torch

import hparams
from DCGAN import models
from gan_utils import data_loading
from DCGAN import pipeline


def get_exp_params(hparams):
    exp_params = {
        "optimizer_fns": (
            (torch.optim.RMSprop, {}),
            (torch.optim.Adam, {"betas": hparams.betas}),
        ),
        "gan_type_sn": (
            (pipeline.DCGAN, [
                {
                    "disc_spectral_norm": False,
                    "disc_bn": True,
                    "disc_steps": 1,
                    "gen_steps": 2,
                }
            ]),
            (pipeline.DCGAN, [
                {
                    "disc_spectral_norm": True,
                    "disc_bn": False,
                    "disc_steps": 2,
                    "epochs": 20,
                }
            ]),
            (pipeline.WGAN,
             [
                 {
                     "disc_spectral_norm": False,
                     "disc_bn": True,
                     "disc_steps": 2,
                     "weight_clipping": 0.01,
                     "grad_penalty": 5.0,
                 }
            ],
            ),
            # (pipeline.WGANGP, [
            #         {
            #             "disc_spectral_norm": False,
            #             "disc_bn": True,
            #             "disc_steps": 5,
            #             "gen_lr": 0.0005,
            #             "disc_lr": 0.001,
            #         },
            #         {
            #             "disc_spectral_norm": False,
            #             "disc_bn": True,
            #             "disc_steps": 5,
            #             "gen_lr": 0.0001,
            #             "disc_lr": 0.001,
            #         },
            #         {
            #             "disc_spectral_norm": False,
            #             "disc_bn": True,
            #             "disc_steps": 10,
            #             "gen_lr": 0.001,
            #             "disc_lr": 0.001,
            #         },
            #         {
            #             "disc_spectral_norm": False,
            #             "disc_bn": False,
            #             "disc_steps": 5,
            #             "gen_lr": 0.0005,
            #             "disc_lr": 0.001,
            #         },
            #         {
            #             "disc_spectral_norm": False,
            #             "disc_bn": False,
            #             "disc_steps": 5,
            #             "gen_lr": 0.0001,
            #             "disc_lr": 0.001,
            #         },
            #         {
            #             "disc_spectral_norm": False,
            #             "disc_bn": False,
            #             "disc_steps": 10,
            #             "gen_lr": 0.001,
            #             "disc_lr": 0.001,
            #         },
            #  ]
            # ),
        ),
        "seeds": (3, ),
    }
    return exp_params


class Config:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def __getitem__(self, key):
        return self.__dict__[key]

    def __setitem__(self, key, value):
        self.__dict__[key] = value


def get_config_hparams():
    module = globals().get("hparams", None)
    return Config(**{key: value for key, value in module.__dict__.items() if not (key.startswith('__') or key.startswith('_'))})


def run_experiments():
    params = get_exp_params(hparams)
    for seed in params["seeds"]:
        for opt in params["optimizer_fns"]:
            for gan_type, hparams_overwrite_list in params["gan_type_sn"]:
                for h_par in hparams_overwrite_list:
                    hparams_cp = get_config_hparams()
                    suffix = ""
                    for k, v in h_par.items():
                        hparams_cp[k] = v
                        suffix += "_" + str(k) + ":" + str(v)
                    spec_norm = hparams_cp["disc_spectral_norm"]
                    model_name = gan_type.__name__ + "SN" if spec_norm else gan_type.__name__
                    hparams_cp["name_suffix"] = suffix
                    run_experiment(
                        model_name=model_name,
                        opt_parms=opt,
                        gan_type=gan_type,
                        seed=seed,
                        hparams=hparams_cp,
                    )


def run_experiment(model_name, opt_parms, gan_type, seed, hparams):
    np.random.seed(seed)
    random.seed(seed)
    torch.manual_seed(seed)

    opt_cls, opt_kwargs = opt_parms
    optimizer_name = opt_cls.__name__
    full_name = model_name + "_" + optimizer_name

    device = torch.device("cuda:0" if (torch.cuda.is_available() and 1 > 0) else "cpu")

    data_loader = data_loading.get_data_loader(
        loader_type=hparams.loader_type,
        data_path=hparams.data_path,
        bs=hparams.bs,
        image_size=hparams.image_size
    )

    generator = models.Generator(channels_shapes=hparams.gen_channels_shapes).to(device)
    discriminator = models.Discriminator(
        channels_shapes=hparams.disc_channels_shapes,
        spectral_norm=hparams.disc_spectral_norm,
        disc_bn=hparams.disc_bn).to(device)

    generator.apply(models.weights_init)
    discriminator.apply(models.weights_init)

    gen_optimizer = opt_cls(generator.parameters(), hparams.gen_lr, **opt_kwargs)
    disc_optimizer = opt_cls(discriminator.parameters(), hparams.disc_lr, **opt_kwargs)

    gen_loss_fn = torch.nn.BCELoss()
    disc_loss_fn = torch.nn.BCELoss()

    gan = gan_type(
        model_name=full_name,
        disc_steps=hparams.disc_steps,
        generator=generator,
        discriminator=discriminator,
        gen_loss_fn=gen_loss_fn,
        disc_loss_fn=disc_loss_fn,
        gen_optimizer=gen_optimizer,
        disc_optimizer=disc_optimizer,
        data_loader=data_loader,
        device=device,
        seed=seed,
        params=hparams,
    )

    gan.train_model()
